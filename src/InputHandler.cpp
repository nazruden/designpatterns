#include "InputHandler.h"
#include "ICommand.h"

InputHandler::InputHandler()
{
	m_pButtonA = std::make_unique<JumpCommand>();
	m_pButtonB = std::make_unique<AttackCommand>();
}

ICommand* InputHandler::HandleInput(Input input)
{
	switch(input)
	{
	case Input::I_Attack:
		return m_pButtonB.get();
	case Input::I_Jump:
		return m_pButtonA.get();
	}
	
	return nullptr;
}

void InputHandler::SwitchCommand()
{
	std::swap(m_pButtonA, m_pButtonB);
}
