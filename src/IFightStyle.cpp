#include "..\Include\IFightStyle.h"
#include <iostream>

KnifeStyle::KnifeStyle() = default;

void KnifeStyle::Fight()
{
	std::cout << " I FIGHT WITH A KNIFE ! " << std::endl;
}

SwordStyle::SwordStyle() = default;

void SwordStyle::Fight()
{
	std::cout << " I FIGHT WITH A SWORD ! " << std::endl;
}
