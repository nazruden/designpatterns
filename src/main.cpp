#include <iostream>
#include "Character.h"
#include "InputHandler.h"
#include "ICommand.h"

int main(int argc, char *argv[])
{
	std::cout << "::>>> DESIGN PATTERNS <<<::" << std::endl << std::endl;

	// CHARACTERS AND FIGHTSTYLE
	std::cout << "::>> Characters and FightStyle" << std::endl << std::endl;

	Character* War = new Warrior();
	War->Fight();

	Character* Rogue = new Thief();
	Rogue->Fight();

	// COMMANDS AND INPUT HANDLER
	std::cout << "::>> Commands and InputHandler" << std::endl << std::endl;
	auto InputHandle = new InputHandler();

	const Input Jump = Input::I_Jump;
	const Input Attack = Input::I_Attack;

	ICommand* Command = InputHandle->HandleInput(Jump);
	if(Command)
	{
		Command->Execute();
	}
	else
	{
		std::cout << " NO JUMP INPUT HANDLED " << std::endl;
	}
	
	Command = InputHandle->HandleInput(Attack);
	if(Command)
	{
		Command->Execute();
	}
	else
	{
		std::cout << " NO ATTACK INPUT HANDLED " << std::endl;
	}

	InputHandle->SwitchCommand();
	std::cout << std::endl << ":> SWITCH COMMANDS " << std::endl << std::endl;

	Command = InputHandle->HandleInput(Jump);
	if (Command)
	{
		Command->Execute();
	}
	else
	{
		std::cout << " NO JUMP INPUT HANDLED " << std::endl;
	}

	Command = InputHandle->HandleInput(Attack);
	if (Command)
	{
		Command->Execute();
	}
	else
	{
		std::cout << " NO ATTACK INPUT HANDLED " << std::endl;
	}
	
	return EXIT_SUCCESS;
}
