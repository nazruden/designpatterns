#pragma once

#include "../Include/Character.h"
#include "IFightStyle.h"
#include <iostream>

void Character::Fight() const
{
	if(m_pFightStyle)
	{
		m_pFightStyle->Fight();
	}
	else
	{
		std::cout << "NO FIGHT STYLE " << std::endl;
	}
}

Warrior::Warrior()
{
	m_pFightStyle = std::make_unique<SwordStyle>();
}

Thief::Thief()
{
	m_pFightStyle = std::make_unique<KnifeStyle>();
}
