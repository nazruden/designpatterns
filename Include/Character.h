#pragma once
#include <memory>

class IFightStyle;

class Character
{
protected:
	std::unique_ptr<IFightStyle> m_pFightStyle = nullptr;

public:
	virtual ~Character() = default;
	virtual void Fight() const;
};

class Warrior : public Character
{
public:
	Warrior();
	~Warrior() = default;
};

class Thief : public Character
{
public:
	Thief();
	~Thief() = default;
};
