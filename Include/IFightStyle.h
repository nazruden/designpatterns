#pragma once

class IFightStyle
{
public:
	virtual ~IFightStyle() = default;
	virtual void Fight() = 0;

};

class KnifeStyle : public IFightStyle
{
public:
	KnifeStyle();
	void Fight() override;
};

class SwordStyle : public IFightStyle
{
public:
	SwordStyle();
	void Fight() override;
};