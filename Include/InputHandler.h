#pragma once
#include <memory>

class ICommand;

enum class Input : __int16
{
	I_Jump,
	I_Attack
};

class InputHandler
{
private:
	std::unique_ptr<ICommand> m_pButtonA = nullptr;
	std::unique_ptr<ICommand> m_pButtonB = nullptr;
	
public:
	InputHandler();
	~InputHandler() = default;

	ICommand* HandleInput(Input input);
	void SwitchCommand();
};
