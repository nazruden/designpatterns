#pragma once
class Color;
class TRay;
class TPoint3D;
class TVec3D;

class ICommand
{
public:
	virtual ~ICommand() = default;
	virtual void Execute() = 0;
};

class JumpCommand : public ICommand
{
public:
	JumpCommand() = default;
	~JumpCommand() = default;

	void Execute() override;
};

class AttackCommand : public ICommand
{
public:
	AttackCommand() = default;
	~AttackCommand() = default;

	void Execute() override;
};

